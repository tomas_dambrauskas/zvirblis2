package lt.akademija.ui.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lt.akademija.entities.Traukinys;
import lt.akademija.entities.Vagonas;
import lt.akademija.entities.repositories.TraukinysRepository;
import lt.akademija.entities.repositories.VagonasRepository;


public class TraukinysListPageBean {
	static final Logger log = LoggerFactory.getLogger(TraukinysListPageData.class);
	
	public static final String NAV_SHOW_ADD_VAGONAS = "show-add-vagonas";
	public static final String NAV_LIST_TRAUKINIAI = "list-traukiniai";
	
	public static class TraukinysListPageData implements Serializable {

		private static final long serialVersionUID = -7847613490719023414L;

		@Valid
		private Traukinys newTraukinys;

		@Valid
		private Traukinys currentTraukinys;
		
		private List<Traukinys> foundTraukinys;

		public void init() {
			newTraukinys = new Traukinys();
			foundTraukinys = new ArrayList<>();
		}


		public List<Traukinys> getFoundTraukinys() {
			return foundTraukinys;
		}

		public void setFoundTraukinys(List<Traukinys> foundTraukinys) {
			this.foundTraukinys = foundTraukinys;
		}

		public Traukinys getCurrentTraukinys() {
			return currentTraukinys;
		}

		public void setCurrentTraukinys(Traukinys currentTraukinys) {
			this.currentTraukinys = currentTraukinys;
		}

		public Traukinys getNewTraukinys() {
			return newTraukinys;
		}

		public void setNewTraukinys(Traukinys newTraukinys) {
			this.newTraukinys = newTraukinys;
		}
	} 



	private TraukinysRepository traukinysRepo;

	private TraukinysListPageData data;


	public String deleteSelected(Traukinys traukinys) {
		if (traukinys == null)
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", "Traukinys yra null?"));
		else {
			traukinysRepo.delete(traukinys);
		}
		return NAV_LIST_TRAUKINIAI;
	}

	
	public String showAddVagonasPage(Traukinys a) {
		log.debug("Will store selected vagonas for later access in Add new Vagonas form: {}", a);
		data.currentTraukinys = a;
		return NAV_SHOW_ADD_VAGONAS;
	}

	public TraukinysListPageData getData() {
		return data;
	}

	public void setData(TraukinysListPageData data) {
		this.data = data;
	}

	public TraukinysRepository getTraukinysRepo() {
		return traukinysRepo;
	}

	public void setTraukinysRepo(TraukinysRepository traukinysRepo) {
		this.traukinysRepo = traukinysRepo;

	}

	public List<Traukinys> getTraukinysList() {
		return traukinysRepo.findAll();
	}
	
	
	
	
}
