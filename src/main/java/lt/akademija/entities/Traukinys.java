package lt.akademija.entities;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.annotation.Generated;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

public class Traukinys implements Serializable {

	private static final long serialVersionUID = -192367291004010121L;
	
	@Id
	private Long numeris;
	private Date pagaminimoData;
	private String imone;
	private String miestas;
	
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Vagonas> vagonai;

	public Long getNumeris() {
		return numeris;
	}

	public void setNumberis(Long numberis) {
		this.numeris = numberis;
	}

	public Date getPagaminimoData() {
		return pagaminimoData;
	}

	public void setPagaminimoData(Date pagaminimoData) {
		this.pagaminimoData = pagaminimoData;
	}

	public String getImone() {
		return imone;
	}

	public void setImone(String imone) {
		this.imone = imone;
	}

	public String getMiestas() {
		return miestas;
	}

	public void setMiestas(String miestas) {
		this.miestas = miestas;
	}

	public List<Vagonas> getVagonai() {
		return vagonai;
	}

	public void setVagonai(List<Vagonas> vagonai) {
		this.vagonai = vagonai;
	}
	
	public void addVagonas(Vagonas a) {
		if (getVagonai() == null) {
			setVagonai(new ArrayList<>());
		} else {
			getVagonai().add(a);
		}
	}
}
