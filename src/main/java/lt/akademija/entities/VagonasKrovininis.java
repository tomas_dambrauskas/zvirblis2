package lt.akademija.entities;

public class VagonasKrovininis extends Vagonas {

	private Long KrovininisGalia;

	public Long getKrovininisGalia() {
		return KrovininisGalia;
	}

	public void setKrovininisGalia(Long KrovininisGalia) {
		this.KrovininisGalia = KrovininisGalia;
	}
	
}
