package lt.akademija.entities;

public class VagonasLokomotyvas extends Vagonas {

	private LokomotyvoTipas lokomotyvoTipas;
	
	private enum LokomotyvoTipas {
		traukiantis, stumiantis
	}

	public LokomotyvoTipas getLokomotyvoTipas() {
		return lokomotyvoTipas;
	}

	public void setLokomotyvoTipas(LokomotyvoTipas lokomotyvoTipas) {
		this.lokomotyvoTipas = lokomotyvoTipas;
	}
	
	
}
