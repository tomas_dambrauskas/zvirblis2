package lt.akademija.entities;

public class VagonasKeleivinis extends Vagonas{
	
	private KeleivinisKlase keleivinisKlase;
	
	private enum KeleivinisKlase {
		pirma, antra, trecia
	}

	public KeleivinisKlase getKeleivinisKlase() {
		return keleivinisKlase;
	}

	public void setKeleivinisKlase(KeleivinisKlase keleivinisKlase) {
		this.keleivinisKlase = keleivinisKlase;
	}
	
}
