package lt.akademija.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

public class Vagonas implements Serializable {

	private static final long serialVersionUID = 6417831368924888360L;

	@Id
	@GeneratedValue
	private Long id;
	
	@NotNull
	@NotBlank
	private BazinisVagonas bazinisVagonas;
	private String gamintojas;
	private int kiekis;
	private Long kaina;
	private int turis;
	
	private enum BazinisVagonas {
		KELEIVINIS, KROVININIS, LOKOMOTYVAS
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BazinisVagonas getBazinisVagonas() {
		return bazinisVagonas;
	}

	public void setBazinisVagonas(BazinisVagonas bazinisVagonas) {
		this.bazinisVagonas = bazinisVagonas;
	}

	public String getGamintojas() {
		return gamintojas;
	}

	public void setGamintojas(String gamintojas) {
		this.gamintojas = gamintojas;
	}

	public int getKiekis() {
		return kiekis;
	}

	public void setKiekis(int kiekis) {
		this.kiekis = kiekis;
	}

	public Long getKaina() {
		return kaina;
	}

	public void setKaina(Long kaina) {
		this.kaina = kaina;
	}

	public int getTuris() {
		return turis;
	}

	public void setTuris(int turis) {
		this.turis = turis;
	}
		
	
}
