package lt.akademija.entities.repositories.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lt.akademija.entities.Traukinys;
import lt.akademija.entities.repositories.TraukinysRepository;


public class TraukinysImpl implements TraukinysRepository {
	static final Logger log = LoggerFactory.getLogger(TraukinysImpl.class);
	
	private EntityManagerFactory entityManagerFactory;

	public void setEntityManagerFactory(EntityManagerFactory emf) {
		this.entityManagerFactory = emf;
	}
	
	private EntityManager getEntityManager() {
		return entityManagerFactory.createEntityManager();
	}
	
	
	@Override
	public List<Traukinys> findAll() {
		
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Traukinys> cq = cb.createQuery(Traukinys.class);
			Root<Traukinys> root = cq.from(Traukinys.class);
			cq.select(root); // we select entity here
			TypedQuery<Traukinys> q = entityManager.createQuery(cq);
			return q.getResultList();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public void save(Traukinys traukinys) {
		
		EntityManager entityManager = getEntityManager();
		try {
			entityManager.getTransaction().begin();
			entityManager.persist(traukinys);
			entityManager.getTransaction().commit();
		} finally {
			entityManager.close();
		}
		
	}

	@Override
	public void delete(Traukinys traukinys) {
		
		EntityManager entityManager = getEntityManager();
		try {
			entityManager.getTransaction().begin();
			traukinys = entityManager.merge(traukinys);
			entityManager.remove(traukinys);
			entityManager.getTransaction().commit();
		} finally {
			entityManager.close();
		}
		
	}

}
