package lt.akademija.entities.repositories.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lt.akademija.entities.Traukinys;
import lt.akademija.entities.Vagonas;
import lt.akademija.entities.repositories.VagonasRepository;

public class VagonasImpl implements VagonasRepository {

	private EntityManagerFactory entityManagerFactory;

	public EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}

	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
	}
	
	@Override
	public List<Vagonas> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void add(Vagonas naujasVagonas) {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Traukinys> cq = cb.createQuery(Traukinys.class);
			Root<Traukinys> root = cq.from(Traukinys.class);
			cq.select(root); // we select entity here
			TypedQuery<Traukinys> q = entityManager.createQuery(cq);
		} finally {
			entityManager.close();
		}
	}

	@Override
	public void delete(Vagonas vagonas) {
		
		EntityManager entityManager = getEntityManager();
		try {
			entityManager.getTransaction().begin();
			entityManager.remove(vagonas);
			entityManager.getTransaction().commit();
		} finally {
			entityManager.close();
		}
		
	}

	private EntityManager getEntityManager() {
		return entityManagerFactory.createEntityManager();
	}
}
