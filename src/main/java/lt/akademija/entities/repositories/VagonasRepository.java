package lt.akademija.entities.repositories;

import java.util.List;

import lt.akademija.entities.Vagonas;

public interface VagonasRepository {
	
	public List<Vagonas> findAll();
	public void add(Vagonas naujasVagonas);
	public void delete(Vagonas vagonas);
	
}
