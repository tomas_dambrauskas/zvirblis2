package lt.akademija.entities.repositories;

import java.util.List;

import lt.akademija.entities.Traukinys;

public interface TraukinysRepository {

	public List<Traukinys> findAll();
	public void save(Traukinys traukinys);
	public void delete(Traukinys traukinys);
	
}
